#include <iostream>
#include <vector>
#include <cstdlib>
#include <stdio.h>
using namespace std;

class Game
{
    struct Pos      // holds the position of empty cell
    {
        int x, y;
        Pos(int x, int y){
            this->x = x;
            this->y = y;
        }
        
        Pos(){
            x = -1;
            y = -1;
        }
    };

    const int WININIG_SCORE = 2048;
    int N, emptyMarker = '-';
    bool win = false;
    Pos ranIdx; // index of random empty cell
    vector<vector<int>> B;

    //Check if any merge possible when B is full
    bool isAdjMergePossible(int i, int j)
    {
        if (i - 1 >= 0 && B[i - 1][j] == B[i][j])
        { // up
            return true;
        }
        if (i + 1 < N && B[i + 1][j] == B[i][j])
        { // down
            return true;
        }
        if (j - 1 >= 0 && B[i][j - 1] == B[i][j])
        { // left
            return true;
        }
        if (j + 1 < N && B[i][j + 1] == B[i][j])
        { // right
            return true;
        }
        return false;
    }

    /* select a random empty position on B.
    */
    Pos getRandomEmptyCell()
    {
        Pos p;
        int rows = B.size(), cols = B[0].size();
        
        //collect all empty cells
        vector<Pos> emptyCells;
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(B[i][j] == -1){
                    emptyCells.push_back(Pos(i, j));
                }
            }
        }
        
        //return any random empty cell
        if(emptyCells.empty() == true){
            return Pos(-1, -1);
        }
        else{
            return emptyCells[rand() % emptyCells.size()];
        }
    }

    /* merge cells in a row towards left .
    */
    bool mergeCellLeft(int i)
    {
        bool isAnyMerged = false;
        for (int j = 1; j < N; j++)
        {
            if (B[i][j] == -1)
                continue;
            if (B[i][j] == B[i][j - 1])
            {
                B[i][j - 1] += B[i][j];
                B[i][j] = -1;
                isAnyMerged = true;
                if (B[i][j - 1] == WININIG_SCORE)
                    win = true;
            }
        }
        return isAnyMerged;
    }

    bool mergeCellRight(int i)
    {
        bool isAnyMerged = false;
        for (int j = N - 2; j >= 0; j--)
        {
            if (B[i][j] == -1)
                continue;
            if (B[i][j] == B[i][j + 1])
            {
                B[i][j + 1] += B[i][j];
                B[i][j] = -1;
                isAnyMerged = true;
                if (B[i][j + 1] == WININIG_SCORE)
                {
                    win = true;
                }
            }
        }
        return isAnyMerged;
    }

    bool mergeCellUp(int c)
    {
        bool isAnyMerged = false;
        for (int r = 1; r < N; r++)
        {
            if (B[r][c] == -1)
                continue;
            if (B[r][c] == B[r - 1][c])
            {
                B[r - 1][c] += B[r][c];
                B[r][c] = -1;
                isAnyMerged = true;
                if (B[r - 1][c] == WININIG_SCORE)
                {
                    win = true;
                }
            }
        }
        return isAnyMerged;
    }

    bool mergeCellDown(int c)
    {
        bool isAnyMerged = false;
        for (int r = N - 2; r >= 0; r--)
        {
            if (B[r][c] == -1)
                continue;
            if (B[r][c] == B[r + 1][c])
            {
                B[r + 1][c] += B[r][c];
                B[r][c] = -1;
                isAnyMerged = true;
                if (B[r + 1][c] == WININIG_SCORE)
                {
                    win = true;
                }
            }
        }
        return isAnyMerged;
    }

    // move left all cell
    bool shiftLeft(int i)
    {
        bool isShf = false; // return true any cell is shifted
        int nextVacant = 0; // next vacant cell
        for (int j = 0; j < N; j++)
        {
            if (B[i][j] != -1)
            {
                swap(B[i][j], B[i][nextVacant]);
                if (j != nextVacant)
                    isShf = true;
                nextVacant += 1;
            }
        }
        return isShf;
    }

    // move right
    bool shiftRight(int i)
    {
        bool isShf = false;
        int nextVacant = N - 1;
        for (int j = N - 1; j >= 0; j--)
        {
            if (B[i][j] != -1)
            {
                swap(B[i][j], B[i][nextVacant]);
                if (j != nextVacant)
                    isShf = true;
                nextVacant -= 1;
            }
        }
        return isShf;
    }

    // shift UP
    bool shiftUp(int c)
    {
        bool isShf = false;
        int nextVacant = 0;
        for (int i = 0; i < N; i++)
        {
            if (B[i][c] != -1)
            {
                swap(B[i][c], B[nextVacant][c]);
                if (i != nextVacant)
                    isShf = true;
                nextVacant += 1;
            }
        }
        return isShf;
    }

    // shift down
    bool shiftDown(int c)
    {
        bool isShf = false;
        int nextVacant = N - 1;
        for (int i = N - 1; i >= 0; i--)
        {
            if (B[i][c] != -1)
            {
                swap(B[i][c], B[nextVacant][c]);
                if (i != nextVacant)
                    isShf = true;
                nextVacant -= 1;
            }
        }
        return isShf;
    }

    void generateTwo()
    {
        Pos p = getRandomEmptyCell();
        B[p.x][p.y] = 2;
        ranIdx = p;
    }

    int getSetBitPos(int t)
    {
        for (int i = 0; i < 32; i++)
        {
            if (1 << i == t)
                return i;
        }
        return 30;
    }

  public:
    Game(int N)
    {
        this->N = N;
        vector<vector<int>> b(N, vector<int>(N, -1));
        B = b;

        //place 2 on empty cell
        Pos p = getRandomEmptyCell();
        B[p.x][p.y] = 2;
        ranIdx = p;
    }

    bool isWin()
    {
        return win;
    }

    bool isLose()
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (B[i][j] == -1) // not lose if any empty cell is present
                    return false;

                //not lose if merge possible
                if (isAdjMergePossible(i, j))
                    return false;
            }
        }
        return true;
    }

    void moveUp()
    {
        bool s1 = 0, mg = 0, s2 = 0;
        for (int j = 0; j < N; j++)
        {
            s1 = shiftUp(j) || s1;
            mg = mergeCellUp(j) || mg;
            s2 = shiftUp(j) || s2;
        }

        if (s1 || mg || s2)
            generateTwo();
    }

    void moveDown()
    {
        bool s1 = 0, mg = 0, s2 = 0;
        for (int j = 0; j < N; j++)
        {
            s1 = shiftDown(j) || s1;
            mg = mergeCellDown(j) || mg;
            s2 = shiftDown(j) || s2;
        }

        if (s1 || mg || s2)
            generateTwo();
    }

    void moveRight()
    {
        bool s1 = 0, mg = 0, s2 = 0;
        for (int i = 0; i < N; i++)
        {
            s1 = shiftRight(i) || s1;
            mg = mergeCellRight(i) || mg;
            s2 = shiftRight(i) || s2;
        }
        if (s1 || mg || s2)
            generateTwo();
    }

    void moveLeft()
    {
        bool s1 = 0, mg = 0, s2 = 0;
        for (int i = 0; i < N; i++)
        {
            s1 = shiftLeft(i) || s1;
            mg = mergeCellLeft(i) || mg;
            s2 = shiftLeft(i) || s2;
        }

        if (s1 || mg || s2)
            generateTwo();
    }

    /**
    * Clears screen using ANSI escape sequences.
    */
    void clear(void)
    {
        //clear the screen.
        printf("\033[2J");

        //puts the cursor at line 0 and column 0.
        printf("\033[%d;%dH", 0, 0);
    }

    int printB()
    {
        printf("\n");
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (B[i][j] != -1)
                {
                    string s = string("\033[0;") + string(to_string(30 + getSetBitPos(B[i][j]))) + string("m");
                    printf("%s", s.c_str());

                    printf("%-5i", B[i][j]);

                    //reset the fg color
                    printf("\033[00m");
                }
                else
                    printf("%-5c", emptyMarker);
            }
            printf("\n");
        }
        return 0;
    }
};

int main()
{
    Game g(4);
    g.printB();
    char key;

    printf("\nWELCOME TO 2048 GAME !\n");
    // use key for playing
    printf("\nUse i, j, k, l for playing .\n Use q to quit game.\n\n");
    printf(" Press i to move up.\n Press k to move down.\n");
    printf(" Press j to move left.\n Press l to move right.\n");
    while (true)
    {
        if (g.isWin())
        {
            printf("YOU WIN :)");
            break;
        }

        if (g.isLose())
        {
            printf("Try Again :(");
            break;
        }
        system("/bin/stty raw"); // put the terminal in raw mode, without buffered
        scanf("%c", &key);
        system("/bin/stty cooked"); // put the terminal in cooked mode, with buffered
        if (key == 'i')
        { // up A
            g.moveUp();
        }
        else if (key == 'k')
        { // down B
            g.moveDown();
        }
        else if (key == 'l')
        { // right C
            g.moveRight();
        }
        else if (key == 'j')
        { // left D
            g.moveLeft();
        }
        else if (key == 'q')
        {
            break;
        }
        else
        {
            printf("Please enter a valid key .");
        }
        g.clear();
        g.printB();
    }
    return 0;
}
