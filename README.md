Play Game 2048 on your terminal .
Steps to play on your computer :
 - Download file Game.cpp .
 - Open terminal where this file is downloaded .
 - Compile it on terminal with command : g++ -std=c++14 Game.cpp
 - Execute command : ./a.out on terminal .
 - Start playing !

![image](Game2048.png)